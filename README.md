# Proj0-Hello
-------------

Author: Mason Jones
Contact: masonj@uoregon.edu
Purpose: A trivial project meant to exercise version control, turn-in, and other
mechanisms which results in the printing of "Hello world".